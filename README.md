# ElkExporter
### Автоматизация экспорта данных из результатов расчетов ELK (файлов *.OUT) в эксель

Создаем папку `MyFolder` перемещаем туда программу ElkExporter.exe, создаем папку `Source`, внутри которой создаем папку со значением состава, например `0.1` и помещаем внутрь файлы ELK которые обычно руками экспортируем в эксель.

Должно получиться примерно так: 

```
C:.
│   ElkExporter.exe
│
└───Sources
    ├───0.1
    │       IDOS.OUT
    │       PDOS_S01_A0001.OUT
    │       PDOS_S01_A0002.OUT
    │       PDOS_S01_A0003.OUT
    │       PDOS_S01_A0004.OUT
    │       PDOS_S02_A0001.OUT
    │       PDOS_S02_A0002.OUT
    │       PDOS_S02_A0003.OUT
    │       PDOS_S02_A0004.OUT
    │       TDOS.OUT
    │
    ├───0.2
    │       IDOS.OUT
    │       PDOS_S01_A0001.OUT
    │       PDOS_S01_A0002.OUT
    │       PDOS_S01_A0003.OUT
    │       PDOS_S01_A0004.OUT
    │       PDOS_S02_A0001.OUT
    │       PDOS_S02_A0002.OUT
    │       PDOS_S02_A0003.OUT
    │       PDOS_S02_A0004.OUT
    │       TDOS.OUT
    │
    ├───0.3
    │       IDOS.OUT
    │       PDOS_S01_A0001.OUT
    │       PDOS_S01_A0002.OUT
    │       PDOS_S01_A0003.OUT
    │       PDOS_S01_A0004.OUT
    │       PDOS_S02_A0001.OUT
    │       PDOS_S02_A0002.OUT
    │       PDOS_S02_A0003.OUT
    │       PDOS_S02_A0004.OUT
    │       TDOS.OUT
    │
```

Запускаем `ElkExporter.exe`, следим за выполнением программы, в конце в каждой папке появится заполненный эксель файл

```
.......
    ├───0.3
    │       0.3.json
    │       0.3.xlsx    <--    заполненный эксель файл
    │       IDOS.OUT
    │       PDOS_S01_A0001.OUT
    │       PDOS_S01_A0002.OUT
    │       PDOS_S01_A0003.OUT
    │       PDOS_S01_A0004.OUT
    │       PDOS_S02_A0001.OUT
    │       PDOS_S02_A0002.OUT
    │       PDOS_S02_A0003.OUT
    │       PDOS_S02_A0004.OUT
    │       TDOS.OUT
........
```