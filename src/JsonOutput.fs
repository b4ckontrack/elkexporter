module JsonOutput
    open System
    open System.Globalization
    open OfficeOpenXml
    open System.IO
    open Newtonsoft.Json
    
    type DosDensity = {
        XEnergy: decimal
        YDensity: decimal
    }
    
    type DosModel = {
        X: decimal
        DElectrons: DosDensity list
    }

    let createJson (pathToExcel: string, name: string) =        
        use file = new ExcelPackage(FileInfo pathToExcel)
        let ws = file.Workbook.Worksheets.["полный FeCo (d)"]
        
        let dElectrons = [
            for ind in [0 .. 1602] do
                let energy = Convert.ToDecimal ws.Cells.[2 + ind, 1].Value
                let density = Convert.ToDecimal ws.Cells.[2 + ind, 7].Value
                
                if (energy <> 0.0m) && (density <> 0.0m) then  
                    yield { XEnergy = energy; YDensity = (density / 4m) } 
        ]
        
        let dos = {
            X = Decimal.Parse (name, CultureInfo.InvariantCulture)
            DElectrons = dElectrons |> List.sortBy (fun d -> d.XEnergy)
        }
        
        let jsonModel = JsonConvert.SerializeObject(dos)
        
        let folder = Directory.GetParent pathToExcel
        
        use writer = File.CreateText (Path.Combine (folder.FullName, name + ".json"))
        
        writer.Write(jsonModel)
