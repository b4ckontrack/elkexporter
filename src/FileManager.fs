module FileManager
    open System
    open System.IO
    open System.Globalization
    
    type FileDosData = {
        Energy: Option<decimal> list
        Dos: Option<decimal> list
    }
    
    type FileDos = {
        Name: string
        Data: FileDosData
    }
    
    type DirectoryDos = {
        Path: string
        XName: string
        Files: FileDos list
    }

    let getFileData filePath: FileDosData =
        let getDecimalsList (strs: seq<string>): Option<decimal> list =                
            let getOptionDecimal (str: string) =               
                match Decimal.TryParse (str, NumberStyles.Any, CultureInfo.InvariantCulture) with
                | true, number -> Some number
                | _ -> None
                
            strs |> Seq.map(getOptionDecimal) |> Seq.toList
                    
        let strings = File.ReadLines (filePath)
                    |> Seq.map(fun x -> x.Split(" ", StringSplitOptions.RemoveEmptyEntries))
                    |> Seq.map(fun s -> if s.Length > 1 then (s.[0], s.[1]) else (String.Empty, String.Empty))
                    
        { Energy = getDecimalsList (strings |> Seq.map(fun x -> fst x))
          Dos = getDecimalsList (strings |> Seq.map(fun x -> snd x)) }


    let getDosFiles() : DirectoryDos[] =
        let getDirectoryOrFileName (path: string) =
            path.Split "\\" |> Array.last
            
        let toFile filePath: FileDos =
            let data = getFileData filePath
            printfn "%s [Dos %i, Energy %i]" filePath data.Dos.Length data.Energy.Length
            { Name = getDirectoryOrFileName filePath
              Data =  data}
            
        let toFilesList (dirPath: string): FileDos list =
            Directory.GetFiles (dirPath, "*.OUT")
                |> Array.filter (fun p -> p.Contains "DOS") |> Array.map toFile |> Array.toList
                
        let toDirectory dirPath =
            { Path = dirPath
              XName = getDirectoryOrFileName dirPath
              Files = toFilesList dirPath}

        Directory.GetDirectories (Path.Combine (Directory.GetCurrentDirectory(), "Sources"))
                    |> Array.map toDirectory
                    