﻿// Learn more about F# at http://fsharp.org

open FileManager
open ExcelManager
open JsonOutput
open OfficeOpenXml
open System.IO
open System

[<EntryPoint>]
let main argv =

    let getDir (dirs: DirectoryDos[], xName: string): DirectoryDos =
        dirs |> Array.find(fun d -> d.XName = xName)
    
    let getFileData (dir: DirectoryDos, fileName: string): FileDosData =
        let file = dir.Files |> List.find(fun f -> f.Name = fileName)
        file.Data
        
        
    let setAll (dir, path) =
        async {
            try
                use excel = new ExcelPackage(FileInfo path)
                            
                let excelWrite (x: int, y: int, values: Option<decimal> list, ws: ExcelWorksheet) =
                    let add (value: Option<decimal>, ind: int) =
                        match value with
                        | Some number ->  ws.Cells.[y + ind, x].Value <- number
                        | _ -> ()
                        
                    for i in 0 .. values.Length - 1 do
                        add (values.[i], i)
                    
                let setPdos (sheatName: string, file: string) =
                    printfn "[x=%s] Writing to %s from %s.." dir.XName sheatName file
                    let ws = excel.Workbook.Worksheets.[sheatName];
                    let pdosData = getFileData (dir, file)
                    excelWrite (2, 2, pdosData.Energy, ws)
                    excelWrite (3, 2, pdosData.Dos, ws)
                    printfn "[x=%s] Writing to %s from %s done." dir.XName sheatName file

                
                let setIdos (sheatName: string, file: string) =
                    printfn "[x=%s] Writing to %s from %s.." dir.XName sheatName file
                    let data = getFileData (dir, file)
                    let ws1 = excel.Workbook.Worksheets.[sheatName];
                    excelWrite (6, 2, data.Dos, ws1)
                    printfn "[x=%s] Writing to %s from %s done." dir.XName sheatName file


                setIdos ("FeCo_1", "IDOS.OUT")
                setPdos ("FeCo_1", "PDOS_S01_A0001.OUT")
                setPdos ("FeCo_1 (2)", "PDOS_S01_A0002.OUT")
                setPdos ("FeCo_1 (3)", "PDOS_S01_A0003.OUT")
                setPdos ("FeCo_1 (4)", "PDOS_S01_A0004.OUT")
                setPdos ("Si-1", "PDOS_S02_A0001.OUT")
                setPdos ("Si-1 (2)", "PDOS_S02_A0002.OUT")
                setPdos ("Si-1 (3)", "PDOS_S02_A0003.OUT")
                setPdos ("Si-1 (4)", "PDOS_S02_A0004.OUT")
                setPdos ("tdos1", "TDOS.OUT")
                
                printfn "[x=%s] Saving xlsx file.." dir.XName
                excel.Save()
                
                use excelSave = new ExcelPackage(FileInfo path)
                excelSave.Workbook.Calculate()
                excelSave.Save()
                
                createJson (path, dir.XName)
                            
                printfn "[x=%s] Done; " dir.XName
            with
            | ex -> printfn "Произошла ошибка при обработке %s: \n\n %O\n\n" dir.XName ex

        } |> Async.Start
            
    
    try  
        let directories = getDosFiles ()

        let xNames = directories |> Array.map(fun d -> d.XName)
        
        printfn "Writing to excel..."
                    
        for xName in xNames do
          let dir = getDir (directories, xName)
          let path = copyExcelFile (dir.Path, dir.XName)
          setAll (dir, path)                                         
        
        Console.ReadLine() |> ignore
        0

    with
    | ex -> printfn "Произошла ошибка: \n\n %O" ex
            Console.ReadLine() |> ignore
            1
        
