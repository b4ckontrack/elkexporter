module ExcelManager
    open System
    open System.IO

    let getTemplateFilePath = Path.Combine (AppDomain.CurrentDomain.BaseDirectory, "Template.xlsx")
    
    let copyExcelFile (newPath, newName) =
        let newPath = Path.Combine (newPath, newName + ".xlsx")
        if File.Exists newPath then
             printfn "Excel file already created at %s" newPath
        else
            File.Copy (getTemplateFilePath, newPath)
            printfn "Create excel at %s" newPath
        newPath
     
       